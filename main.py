#!/usr/bin/env python3

from kwadrat import kwadrat, distance
import pygame
pygame.init()

WINDOW_SIZE = 500
SQUARE_SIZE = 10 
BORDER = 500
MULTIPLY_BORDER = 100

kwadraty_red = []
kwadraty_blue = []

window = pygame.display.set_mode((WINDOW_SIZE, WINDOW_SIZE))
run = True

# set squares 
k1 = kwadrat(100, 200, "red", SQUARE_SIZE, "E", 1, MULTIPLY_BORDER)
k2 = kwadrat(400, 200, "red", SQUARE_SIZE, "W", 2, MULTIPLY_BORDER)
kwadraty_red.append(k1)
kwadraty_red.append(k2)

k1 = kwadrat(100, 300, "blue", SQUARE_SIZE, "E", 1, MULTIPLY_BORDER)
k2 = kwadrat(400, 300, "blue", SQUARE_SIZE, "W", 2, MULTIPLY_BORDER)
kwadraty_blue.append(k1)
kwadraty_blue.append(k2)

while run:

    if not kwadraty_blue:
        run = False

    if not kwadraty_red:
        run = False

    pygame.time.delay(10)
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    window.fill((0,0,0))

    buff_red = [None] * len(kwadraty_red)
    for i in range(len(kwadraty_red)):
        buff_red[i] = kwadraty_red[i]

    # Analyze red squares
    for element in buff_red: # Check if square reaches a wall
        if element.x == (BORDER-SQUARE_SIZE) or element.y == (BORDER-SQUARE_SIZE) or element.x == 0 or element.y == 0:
            element.random_direction()

        for opponent in kwadraty_blue: # Check if square meets opponent
            if distance(element,opponent) <= (SQUARE_SIZE/2 + 1):
                kwadraty_red.remove(element)
                kwadraty_blue.remove(opponent)
                break

        for friend in buff_red: # Check if square meets friend
            if distance(element, friend) <= (SQUARE_SIZE/2 + 1) and element.id_number != friend.id_number \
            and element.multiply_points > MULTIPLY_BORDER and friend.multiply_points > MULTIPLY_BORDER:
                k_new = kwadrat(element.x, element.y, "red", SQUARE_SIZE, "N", kwadraty_red[-1].id_number + 1)
                kwadraty_red.append(k_new)
                element.zero()
                friend.zero()

        element.intelligent_move(BORDER)


    buff_blue = [None] * len(kwadraty_blue)
    for i in range(len(kwadraty_blue)):
        buff_blue[i] = kwadraty_blue[i]

    # Analyze blue squares
    for element in buff_blue:
        if element.x == (BORDER-SQUARE_SIZE) or element.y == (BORDER-SQUARE_SIZE) or element.x == 0 or element.y == 0:
            element.random_direction()

        for opponent in kwadraty_red:
            if distance(element,opponent) <= (SQUARE_SIZE/2 + 1):
                kwadraty_red.remove(element)
                kwadraty_blue.remove(opponent)
                break

        for friend in buff_blue:
            if distance(element,friend) <= (SQUARE_SIZE/2 + 1) and element.id_number != friend.id_number \
            and element.multiply_points > MULTIPLY_BORDER and friend.multiply_points > MULTIPLY_BORDER:
                k_new = kwadrat(element.x, element.y, "blue", SQUARE_SIZE, "N",kwadraty_red[-1].id_number + 1)
                kwadraty_blue.append(k_new)
                element.zero()
                friend.zero()

        element.intelligent_move(BORDER)

    #Draw
    for element in kwadraty_red:
        element.move()
        pygame.draw.rect(window, (255, 0, 0), (element.x, element.y, SQUARE_SIZE, SQUARE_SIZE))
        

    for element in kwadraty_blue:
        element.move()
        pygame.draw.rect(window, (0, 0, 255), (element.x, element.y, SQUARE_SIZE, SQUARE_SIZE))


    pygame.display.update()


pygame.quit()

