#!/usr/bin/env python3 

from enum import Enum

class direction(Enum):
    N = 0
    E = 1
    S = 2
    W = 3
    NE = 4
    NW = 5
    SE = 6
    SW = 7


