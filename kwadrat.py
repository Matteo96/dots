#!/usr/bin/env python3

import random
import math


class kwadrat:
    def __init__(self, x, y, color, size, direction, id_number, multiply_points=0):
        self.x = x
        self.y = y
        self.color = color
        self.size = size
        self.direction = direction
        self.id_number = id_number
        self.multiply_points = multiply_points
 
    def move(self):
        if self.direction == "N":
            self.y -= 1
        elif self.direction == "E":
            self.x += 1
        elif self.direction == "S":
            self.y += 1
        elif self.direction == "W":
            self.x -= 1
        elif self.direction == "NE":
            self.x += 1
            self.y -= 1
        elif self.direction == "NW":
            self.x -= 1
            self.y -= 1
        elif self.direction == "SE":
            self.x += 1
            self.y += 1
        elif self.direction == "SW":
            self.x -= 1
            self.y += 1
        else:
            print("Direction Error")

        if self.multiply_points <= 100:
            self.multiply_points += 1


    def precise_move(self, distance=1):
        if not(math.isnan(distance)):
            if self.direction == "N":
                self.y -= distance
            elif self.direction == "E":
                self.x += distance
            elif self.direction == "S":
                self.y += distance
            elif self.direction == "W":
                self.x -= distance
            elif self.direction == "NE":
                self.x += distance
                self.y -= distance
            elif self.direction == "NW":
                self.x -= distance
                self.y -= distance
            elif self.direction == "SE":
                self.x += distance
                self.y += distance
            elif self.direction == "SW":
                self.x -= distance
                self.y += distance
            else:
                print("Direction Error")    
        else:
            print("Distance is not a number")
        
    
    def set_direction(self, direction):
        self.direction = direction


    def random_direction(self):
        directions = ["N", "E", "S", "W", "NE", "NW", "SE", "SW"]
        
        new_direction = self.direction
        while new_direction == self.direction:
            new_direction_index = random.randrange(len(directions))
            
            if directions[new_direction_index] != self.direction:
                self.set_direction(directions[new_direction_index])


    def get_coordinates(self):
        return (self.x, self.y)


    def get_direction(self):
        return self.direction


    def bounce_down(self):
        directions = ["E", "S", "W", "SE", "SW"]
        new_direction_index = random.randrange(len(directions))
        self.set_direction(directions[new_direction_index])


    def bounce_up(self):
        directions = ["N", "E", "W", "NE", "NW"]
        new_direction_index = random.randrange(len(directions))
        self.set_direction(directions[new_direction_index])


    def bounce_left(self):
        directions = ["N", "S", "W", "NW", "SW"]
        new_direction_index = random.randrange(len(directions))
        self.set_direction(directions[new_direction_index])


    def bounce_right(self):
        directions = ["N", "E", "S", "NE", "SE"]
        new_direction_index = random.randrange(len(directions))
        self.set_direction(directions[new_direction_index])


    def corner_left_up(self):
        directions = ["S", "E", "SE"]
        new_direction_index = random.randrange(len(directions))
        self.set_direction(directions[new_direction_index])
        

    def corner_left_down(self):
        directions = ["N", "E", "NE"]
        new_direction_index = random.randrange(len(directions))
        self.set_direction(directions[new_direction_index])


    def corner_right_down(self):
        directions = ["N", "W", "NW"]
        new_direction_index = random.randrange(len(directions))
        self.set_direction(directions[new_direction_index])


    def corner_right_up(self):
        directions = ["S", "W", "SW"]
        new_direction_index = random.randrange(len(directions))
        self.set_direction(directions[new_direction_index])

    
    def intelligent_move(self, border, distance=1):
        if self.y == 0 and self.x > 0 and self.x < border-self.size:
            self.bounce_down()
            self.precise_move(distance)
        elif self.y == border-self.size and self.x > 0 and self.x < border-self.size:
            self.bounce_up()
            self.precise_move(distance)
        elif self.x == 0 and self.y > 0 and self.y < border-self.size:
            self.bounce_right()
            self.precise_move(distance)
        elif self.x == border-self.size and self.y > 0 and self.y < border-self.size:   
            self.bounce_left()
            self.precise_move(distance)
        elif self.x == 0 and self.y == 0:
            self.corner_left_up()
            self.precise_move(distance)
        elif self.x == border-self.size and self.y == 0:
            self.corner_right_up()
            self.precise_move(distance)
        elif self.x == 0 and self.y == border-self.size:
            self.corner_left_down()
            self.precise_move(distance)
        elif self.x == border-self.size and self.y == border-self.size:
            self.corner_right_down()
            self.precise_move(distance)


    def test_data(self):
        print("Coordinates" + str(self.get_coordinates()))
        print("Movement:")
        self.move()
        self.move()
        print("Movement" + str(self.get_coordinates()))
        print("Direction: " + str(self.get_direction()))
        self.set_direction("W")
        print("Direction: " + str(self.get_direction()))
        self.random_direction()
        print("Direction: " + str(self.get_direction()))

        
        
    def right_wall(self):
        return self.x + self.size

    def left_wall(self):
        return self.x

    def top_wall(self):
        return self.y

    def bottom_wall(self):
        return self.y + self.size

    def zero(self):
        self.multiply_points = 0
        
#def accidental_direction(d1, d2):
        #  |  N  E  S  W  NE  NW  SE  SW
        #--|----------------------------
        #N |     W  E  W  SW  SE  SW  SE  
        #E |  W     N  N  SW  SW  NW  NE
        #S |  E  N     E  NW  NE  NW  NE
        #W |  E  N  N     SE  SE  NW  NE
        #NE|  SW SW NW SE     SW  NW  NW
        #NW|  SE SW NE SE SW      NE  SE
        #SE|  SW NW NW NW NW  NE      NW
        #SW|  SE NE NE NE NW  SE  NW


def distance(A, B):
    d = math.sqrt( math.pow((B.x-A.x),2) + math.pow((B.y-A.y),2))
    return d

